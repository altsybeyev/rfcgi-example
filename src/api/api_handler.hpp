
#include <functional>
#include <list>
#include <map>
#include <memory>

#include <serreflection/read_url.hpp>

#include <rfcgi/api_handler.hpp>

namespace api
{
    class ApiHandler : public rfcgi::BaseApiHandler
    {
    public:
        ApiHandler();
    };

} // namespace rfcgi
