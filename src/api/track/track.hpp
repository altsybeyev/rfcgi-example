#ifndef API_track
#define API_track

#include <rfcgi/iendpoints_handler.hpp>

namespace api
{
namespace track
{
namespace data_models
{
namespace query
{         
struct Get;
struct DeleteMethod;
struct Post;
struct Patch;
struct GetByUserId;

}
namespace payload
{

}  
}
class Handler : public rfcgi::IEndpointsHandler
{
public:   
    Handler();
    
    rfcgi::HttpResponse get(const std::unique_ptr<std::string>& id_user,   const std::unique_ptr<data_models::query::Get>& query,const std::string&  payload);

    rfcgi::HttpResponse delete_method(const std::string& id_user,   const std::unique_ptr<data_models::query::DeleteMethod>& query,const std::string&  payload);

    rfcgi::HttpResponse post(const std::string& id_user,   const std::unique_ptr<data_models::query::Post>& query,const std::string&  payload);

    rfcgi::HttpResponse patch(const std::string& id_user,   const std::unique_ptr<data_models::query::Patch>& query,const std::string&  payload);

    rfcgi::HttpResponse get_by_user_id(const std::unique_ptr<std::string>& id_user,   const std::unique_ptr<data_models::query::GetByUserId>& query,const std::string&  payload);

};
}
}

#endif