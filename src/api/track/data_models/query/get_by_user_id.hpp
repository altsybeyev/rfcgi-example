#ifndef API_TRACK_QUERY_GET_BY_USER_ID_HPP
#define API_TRACK_QUERY_GET_BY_USER_ID_HPP

#include <serreflection/defines.hpp>

namespace api
{
namespace track
{
namespace data_models
{
namespace query
{        

struct GetByUserId
{  
    std::string id;
};
}
}
}
}

SERIALIZIBLE_STRUCT(api::track::data_models::query::GetByUserId, srfl::CheckModes::FATAL, 
(std::string, id, DEF_D()))

#endif