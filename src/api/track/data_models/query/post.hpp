#ifndef API_TRACK_QUERY_POST_HPP
#define API_TRACK_QUERY_POST_HPP

#include <serreflection/defines.hpp>

namespace api
{
namespace track
{
namespace data_models
{
namespace query
{        

struct Post
{ 
};
}
}
}
}

SERIALIZIBLE_EMPTY_STRUCT(api::track::data_models::query::Post)

#endif