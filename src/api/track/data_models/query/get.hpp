#ifndef API_TRACK_QUERY_GET_HPP
#define API_TRACK_QUERY_GET_HPP

#include <serreflection/defines.hpp>

namespace api
{
namespace track
{
namespace data_models
{
namespace query
{        
enum class GetEnumTest 
{  en,  ru, }; 

struct Get
{  
    std::vector<std::string> ids; 
    GetEnumTest test;
};
}
}
}
}

SERIALIZIBLE_ENUM(api::track::data_models::query::GetEnumTest, (en)(ru))
SERIALIZIBLE_STRUCT(api::track::data_models::query::Get, srfl::CheckModes::FATAL, 
(std::vector<std::string>, ids, DEF_D()) 
(api::track::data_models::query::GetEnumTest, test,"qwe", INF_D()))

#endif