#ifndef API_TRACK_QUERY_DELETE_METHOD_HPP
#define API_TRACK_QUERY_DELETE_METHOD_HPP

#include <serreflection/defines.hpp>

namespace api
{
namespace track
{
namespace data_models
{
namespace query
{        

struct DeleteMethod
{  
    std::vector<std::string> ids;
};
}
}
}
}

SERIALIZIBLE_STRUCT(api::track::data_models::query::DeleteMethod, srfl::CheckModes::FATAL, 
(std::vector<std::string>, ids, DEF_D()))

#endif