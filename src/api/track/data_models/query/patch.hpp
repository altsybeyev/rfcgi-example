#ifndef API_TRACK_QUERY_PATCH_HPP
#define API_TRACK_QUERY_PATCH_HPP

#include <serreflection/defines.hpp>

namespace api
{
namespace track
{
namespace data_models
{
namespace query
{        

struct Patch
{  
    std::string id;
};
}
}
}
}

SERIALIZIBLE_STRUCT(api::track::data_models::query::Patch, srfl::CheckModes::FATAL, 
(std::string, id, DEF_D()))

#endif