#include "track.hpp"

#include <common_tools/boostlog.hpp>

#include "data_models/query/get.hpp"
#include "data_models/query/delete_method.hpp"
#include "data_models/query/post.hpp"
#include "data_models/query/patch.hpp"
#include "data_models/query/get_by_user_id.hpp"





namespace api
{
namespace track
{
    Handler::Handler()
    {
        REGISTER_OPERATION_WITH_USER("get", Handler::get,std::unique_ptr<std::string>,  data_models::query::Get,std::string )
        REGISTER_OPERATION_WITH_USER("delete", Handler::delete_method,std::string,  data_models::query::DeleteMethod,std::string )
        REGISTER_OPERATION_WITH_USER("post", Handler::post,std::string,  data_models::query::Post,std::string )
        REGISTER_OPERATION_WITH_USER("patch", Handler::patch,std::string,  data_models::query::Patch,std::string )
        REGISTER_OPERATION_WITH_USER("get_byUserId", Handler::get_by_user_id,std::unique_ptr<std::string>,  data_models::query::GetByUserId,std::string )
              
    }
}
}