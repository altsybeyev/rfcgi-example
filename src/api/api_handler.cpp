
#include "api_handler.hpp"

 
#include "track/track.hpp"
 
#include "auth/auth.hpp"


namespace api
{
    ApiHandler::ApiHandler()
    {
 
            register_endpoints_handler("track",std::make_unique<track::Handler>());
 
            register_endpoints_handler("auth",std::make_unique<auth::Handler>());
    }
} 
// namespace rfcgi