#ifndef API_AUTH_PAYLOAD_POST_TMP_PASSWORD_HPP
#define API_AUTH_PAYLOAD_POST_TMP_PASSWORD_HPP

#include <serreflection/defines.hpp>

namespace api
{
namespace auth
{
namespace data_models
{
namespace payload
{        
enum class PostTmpPasswordEnumLang 
{  en,  ru, }; 

struct PostTmpPassword
{  
    std::string email; 
    PostTmpPasswordEnumLang lang; 
    bool reset_password;
};
}
}
}
}

SERIALIZIBLE_ENUM(api::auth::data_models::payload::PostTmpPasswordEnumLang, (en)(ru))
SERIALIZIBLE_STRUCT(api::auth::data_models::payload::PostTmpPassword, srfl::CheckModes::FATAL, 
(std::string, email, DEF_D()) 
(api::auth::data_models::payload::PostTmpPasswordEnumLang, lang,"en", INF_D()) 
(bool, reset_password, DEF_D()))

#endif