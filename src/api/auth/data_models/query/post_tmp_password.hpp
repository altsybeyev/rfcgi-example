#ifndef API_AUTH_QUERY_POST_TMP_PASSWORD_HPP
#define API_AUTH_QUERY_POST_TMP_PASSWORD_HPP

#include <serreflection/defines.hpp>

namespace api
{
namespace auth
{
namespace data_models
{
namespace query
{        

struct PostTmpPassword
{ 
};
}
}
}
}

SERIALIZIBLE_EMPTY_STRUCT(api::auth::data_models::query::PostTmpPassword)

#endif