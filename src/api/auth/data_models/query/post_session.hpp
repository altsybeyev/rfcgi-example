#ifndef API_AUTH_QUERY_POST_SESSION_HPP
#define API_AUTH_QUERY_POST_SESSION_HPP

#include <serreflection/defines.hpp>

namespace api
{
namespace auth
{
namespace data_models
{
namespace query
{        

struct PostSession
{ 
};
}
}
}
}

SERIALIZIBLE_EMPTY_STRUCT(api::auth::data_models::query::PostSession)

#endif