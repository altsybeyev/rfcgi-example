#ifndef API_auth
#define API_auth

#include <rfcgi/iendpoints_handler.hpp>

namespace api
{
namespace auth
{
namespace data_models
{
namespace query
{         
struct PostSession;
struct PostTmpPassword;

}
namespace payload
{
struct PostTmpPassword;
}  
}
class Handler : public rfcgi::IEndpointsHandler
{
public:   
    Handler();
    
    rfcgi::HttpResponse post_session(  const std::unique_ptr<data_models::query::PostSession>& query,const std::string&  payload);

    rfcgi::HttpResponse post_tmp_password(  const std::unique_ptr<data_models::query::PostTmpPassword>& query, const std::unique_ptr<data_models::payload::PostTmpPassword>&  payload);

};
}
}

#endif