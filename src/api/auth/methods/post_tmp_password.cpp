#include "../auth.hpp"

#include "../data_models/query/post_tmp_password.hpp"
#include "../data_models/payload/post_tmp_password.hpp"


namespace api
{
namespace auth
{
rfcgi::HttpResponse Handler::post_tmp_password(  const std::unique_ptr<data_models::query::PostTmpPassword>& query, const std::unique_ptr<data_models::payload::PostTmpPassword>&  payload)
{
    BL_FTRACE();
}
}
}