#include "auth.hpp"

#include <common_tools/boostlog.hpp>

#include "data_models/query/post_session.hpp"
#include "data_models/query/post_tmp_password.hpp"


#include "data_models/payload/post_tmp_password.hpp"


namespace api
{
namespace auth
{
    Handler::Handler()
    {
        REGISTER_OPERATION("post_session", Handler::post_session, data_models::query::PostSession,std::string )
        REGISTER_OPERATION("post_tmpPassword", Handler::post_tmp_password, data_models::query::PostTmpPassword, std::unique_ptr<data_models::payload::PostTmpPassword> )
              
    }
}
}