#include "api/api_handler.hpp"
#include <rfcgi/iendpoints_handler.hpp>

int main()
{
    api::ApiHandler handler;
    handler.execute(nullptr, "get", "/api/track", "", "", nullptr);
    return 0;
}